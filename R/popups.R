plotpop = function(ltla, date_min, date_max, txt, ylims=c(0,2)){
    plot.Rt(ltla,date_min=date_min,date_max=date_max,txt=txt,ylims=ylims, title.cex=1)

    ltla = ltla[ltla$date_end >= date_min & ltla$date_end <=date_max,]
    
    t1 = ltla$exceed1 > .9
    t2 = ltla$exceed1 > .95
    T1 = ltla$date_end[t2]
    T2 = ltla$date_end[t1 & !t2]

    t1b = ltla$below1 > .9
    t2b = ltla$below1 > .95
    T1b = ltla$date_end[t2b]
    T2b = ltla$date_end[t1b & !t2b]

    if(length(T1)>0){
        axis(3, at=T1, col="red", pos=1, lwd=0,lwd.ticks=3, labels=FALSE )
    }
    if(length(T2)>0){
        axis(3, at=T2, col="orange", pos=1, lwd=0,lwd.ticks=1, labels=FALSE )
    }
    if(length(T1b)>0){
        axis(1, at=T1b, col="darkgreen", pos=1, lwd=0,lwd.ticks=3, labels=FALSE )
    }
    if(length(T2b)>0){
        axis(1, at=T2b, col="blue", pos=1, lwd=0,lwd.ticks=1, labels=FALSE )
    }
}
